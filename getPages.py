# Takes a list of listing pages, and turns them into a CSV of individual psychologist links. finalScraper.py uses this CSV to get the info from each psychologist

import requests
import multiprocessing
import sys
import csv
from bs4 import BeautifulSoup

sys.setrecursionlimit(100000)

# Necessary request variables to pass to Beautiful Soup HTML parser
initial_request_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }

url_list = []
# url_list = [
#     'https://www.psychologytoday.com/us/therapists/ca/los-angeles-county?sid=1523392789.1252_25946&rec_next=1'
# ]

with open("urls.csv",'rb') as urls_file:
    url_reader = csv.reader(urls_file,)
    # Iterate through csv file
    for row in url_reader:
        psychology_today_url = ', '.join(row)
        url_list.append(psychology_today_url)

# # Execute psychologist page finder for each url in the csv file
def psychologist_pages_finder(url_list):

    page_url                = url_list
    request                 = requests.get(page_url, headers=initial_request_headers)
    page_html               = request.content
    initial_soup            = BeautifulSoup(page_html, 'html.parser')
    psychologist_page_links = initial_soup.find_all(class_='result-name')

    for links in psychologist_page_links:
        final_psychologist_page_links = links.get('href')

        with open("allUrls.csv",'ab') as resultFile:
            quoting = csv.QUOTE_ALL
            wr = csv.writer(resultFile, quoting)
            wr.writerow([final_psychologist_page_links])

#     if __name__ == '__main__':
map(psychologist_pages_finder, url_list)