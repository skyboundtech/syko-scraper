# Takes a the list of all psychologist links and retrieves the info from all 24k.
# Puts the information into "finalInfo."

import requests
import multiprocessing
import sys
import csv
from bs4 import BeautifulSoup
import requesocks

sys.setrecursionlimit(100000)

# Necessary request variables to pass to Beautiful Soup HTML parser
initial_request_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }

# href_links = [
#     'https://www.psychologytoday.com/us/therapists/cherri-davies-west-hills-ca/181549?sid=1523392789.1252_25946&county=Los+Angeles+County&state=CA&rec_next=1&ref=1&tr=ResultsName',
# ]



href_links = []
with open("allUrls.csv",'rb') as urls_file:
    url_reader = csv.reader(urls_file,)
    # Iterate through csv file
    for row in url_reader:
        psychology_today_url = ', '.join(row)
        href_links.append(psychology_today_url)

psychologist_page_link_http_request = ''
psychologist_page_html_content      = ''
session = requests.session()
session.proxies = {}
session.proxies['http'] = 'socks5h://localhost:9050'
session.proxies['https'] = 'socks5h://localhost:9050'

def get_therapist_info(href_links):
    
    # Initiate request variables for individual psychologist pages
    psychologist_page_link_http_request_headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }


    # Loop through psychologist page links and do something
    # for links in href_links:

    psychologist_page_link = href_links

    psychologist_page_link_http_request = session.get(psychologist_page_link, headers=psychologist_page_link_http_request_headers)

    psychologist_page_html_content      = psychologist_page_link_http_request.content
    psychologist_page_soup              = BeautifulSoup(psychologist_page_html_content, 'html.parser')

        # Get psychologist name
    psychologist_name                       = psychologist_page_soup.find('h1',     itemprop='name')
    psychologist_title                      = psychologist_page_soup.find('div',    class_='profile-title')
    # Get psychologist phone number
    psychologist_phone_number               = psychologist_page_soup.find('a',      class_='phone-number')
    # Get psychologist address
    psychologist_street_address             = psychologist_page_soup.find('span',   itemprop='streetAddress')
    psychologist_address_locality           = psychologist_page_soup.find('span',   itemprop='addressLocality')
    psychologist_address_region             = psychologist_page_soup.find('span',   itemprop='addressRegion')
    psychologist_address_postal_code        = psychologist_page_soup.find('span',   itemprop='postalcode')
    psychologist_specialties                = psychologist_page_soup.find('ul',     class_='specialties-list')

    psychologist_attributes_issues          = psychologist_page_soup.find('div', class_='spec-list attributes-issues')

    

    line_break = '***********************************************'

    # If statements

    # Name
    if psychologist_name:
        psychologist_name = psychologist_name.get_text(strip=True).encode('UTF-8')
    else:
        psychologist_name = 'No main name to scrape'

    # Title
    if psychologist_title:
        psychologist_title = psychologist_title.get_text(strip=True).encode('UTF-8')
    else:
        psychologist_title = 'No title to scrape.'

    # Phone number
    if psychologist_phone_number:
        psychologist_phone_number = psychologist_phone_number.get_text(strip=True).encode('UTF-8')
    else:
        psychologist_phone_number = 'No phone number to scrape.'

    # Street Address
    if psychologist_street_address:
        psychologist_street_address = psychologist_street_address.get_text(strip=True).encode('UTF-8')
    else:
        psychologist_street_address = 'N/A'
    # Address locality
    if psychologist_address_locality:
        psychologist_address_locality = psychologist_address_locality.get_text(strip=True).encode('UTF-8')
    else:
        psychologist_address_locality = 'N/A'
    # Address regions
    if psychologist_address_region:
        psychologist_address_region    = psychologist_address_region.get_text(strip=True).encode('UTF-8')
    else:
        psychologist_address_region    = 'N/A'
    # Address postal code
    if psychologist_address_postal_code:
        psychologist_address_postal_code = psychologist_address_postal_code.get_text(strip=True).encode('UTF-8')
    else:
        psychologist_address_postal_code = 'N/A'
    # Psychologist specialties
    if psychologist_specialties:
        psychologist_specialties        = psychologist_specialties.get_text( ', ', strip=True).encode('UTF-8')
    else:
        psychologist_specialties        = 'N/A'
    
    if psychologist_attributes_issues:
        psychologist_attributes_issues = psychologist_attributes_issues.get_text(' *', strip=True).encode('UTF-8')
    else:
        psychologist_attributes_issues = 'N/A'

        
        

    # print psychologist_address_locality + ', ' + psychologist_address_region + ', ' + psychologist_address_postal_code
    # print psychologist_specialties

    # print psychologist_attributes_issues

    print line_break

    with open("finalInfo.csv",'ab') as resultFile:
        field_names = [
            'name',
            'title',
            'phone_number',
            'streetAddress',
            'addressLocality',
            'addressRegion',
            'postalCode',
            'specialties',
            'attributesIssues',
            'url'
        ]
        wr = csv.DictWriter(resultFile, fieldnames=field_names)
        wr.writerow({
        'name'              : psychologist_name,
        'title'             : psychologist_title,
        'phone_number'      : psychologist_phone_number,
        'streetAddress'     : psychologist_street_address,
        'addressLocality'   : psychologist_address_locality,
        'addressRegion'     : psychologist_address_region,
        'postalCode'        : psychologist_address_postal_code,
        'specialties'       : psychologist_specialties,
        'attributesIssues'  : psychologist_attributes_issues,
        'url'               : psychologist_page_link
        })

      
map(get_therapist_info, href_links)

# pool = multiprocessing.Pool(processes=50)
# pool.map(get_therapist_info, href_links)