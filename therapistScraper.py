import requests
from bs4 import BeautifulSoup
import multiprocessing

# Necessary request variables to pass to Beautiful Soup HTML parser
initial_request_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }

psychchology_today_url  = 'https://www.psychologytoday.com/us/therapists/ca/los-angeles-county/'
request                 = requests.get(psychchology_today_url, headers=initial_request_headers)
page_html               = request.content

# Initialize scraper
initial_soup = BeautifulSoup(page_html, 'html.parser')

# Find href links
href_links = initial_soup.find_all(class_='result-name')


psychologist_name = ''

def get_therapist_info():

    # Initiate request variables for individual psychologist pages
    psychologist_page_link_http_request_headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
    }

    psychologist_page_link_http_request = ''
    psychologist_page_html_content      = ''
    counter                             = 1

    # Loop through psychologist page links and do something
    for links in href_links:    

        # Conditional for limiting testing output
        if counter <= 1:
            psychologist_page_link = links.get('href')

            psychologist_page_link_http_request = requests.get(psychologist_page_link, headers=psychologist_page_link_http_request_headers)
            psychologist_page_html_content      = psychologist_page_link_http_request.content
            psychologist_page_soup              = BeautifulSoup(psychologist_page_html_content, 'html.parser')

            # Get psychologist name
            psychologist_name                       = psychologist_page_soup.find('h1',     itemprop='name').get_text(strip=True)
            psychologist_title                      = psychologist_page_soup.find('div',    class_='profile-title').get_text(strip=True)
            # Get psychologist phone number
            psychologist_phone_number               = psychologist_page_soup.find('a',      class_='phone-number').get_text(strip=True)
            # Get psychologist address
            psychologist_street_address             = psychologist_page_soup.find('span',   itemprop='streetAddress')
            psychologist_address_locality           = psychologist_page_soup.find('span',   itemprop='addressLocality').get_text(strip=True)
            psychologist_address_region             = psychologist_page_soup.find('span',   itemprop='addressRegion').get_text(strip=True)
            psychologist_address_postal_code        = psychologist_page_soup.find('span',   itemprop='postalcode').get_text(strip=True)
            psychologist_specialties                = psychologist_page_soup.find('ul',     class_='specialties-list').get_text( ', ', strip=True)

            psychologist_attributes_issues          = psychologist_page_soup.find('div', class_='spec-list attributes-issues').get_text(' *', strip=True)

            line_break = '***********************************************'
            
            # print psychologist_name
            # print psychologist_title
            # print psychologist_phone_number

            # if psychologist_street_address:
            #     print psychologist_street_address.get_text(strip=True)
            # else:
            #     print 'No street address to scrape'

            # print psychologist_address_locality + ', ' + psychologist_address_region + ', ' + psychologist_address_postal_code
            # print psychologist_specialties

            print psychologist_attributes_issues
            print line_break
            
            counter += 1

get_therapist_info()